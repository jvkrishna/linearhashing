package cyclients.linearhashing;

import cyclients.linearhashing.util.ByteUtils;

/**
 * Utility class for handling directory page headers.
 * 
 * @author Vamsi Krishna J
 *
 */
public class DirectoryPage {

	public static final int DEFAULT_HEADER_SIZE = 16;
	public static final int DEFAUL_PAGE_SIZE = 1024;
	public static final int DEFAULT_RECORD_SIZE = 8;

	/**
	 * Page header enumerations.
	 * 
	 * @author Vamsi Krishna J
	 *
	 */
	public enum DirectoryPageHeader {
		HEADER_SIZE(0), CURRENT_PAGE(4), NEXT_PAGE(8), PAGE_SIZE(12);
		private int offset;

		private DirectoryPageHeader(int offset) {
			this.offset = offset;
		}

		public int getOffset() {
			return offset;
		}
	}

	public static void setHeader(byte[] buffer, DirectoryPageHeader headerAttr, int value) {
		ByteUtils.intToByteArray(value, buffer, headerAttr.getOffset());
	}

	public static int getHeader(byte[] buffer, DirectoryPageHeader headerAttr) {
		return ByteUtils.byteArrayToInt(buffer, headerAttr.getOffset());
	}
}
