package cyclients.linearhashing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.BiConsumer;
import java.util.function.ToDoubleFunction;

import cyclients.linearhashing.entities.LinearHashConfig;
import cyclients.linearhashing.util.ApplicationConfig;
import cyclients.linearhashing.util.ByteUtils;
import cyclients.linearhashing.util.ChainIterator;
import cyclients.linearhashing.util.LinearHashUtils;
import cyclients.linearhashing.util.PageHeader;
import cyclients.linearhashing.util.PageHeader.HEADER_ATTR;
import storagemanager.StorageManagerClient;

/**
 * Handler class for executing operations.
 * 
 * @author Vamsi Krishna J
 *
 */
public class LinearHashFile {

	private ApplicationConfig applicationConfig = ApplicationConfig.getInstance();
	private Tuple tuple;
	private LinearHashConfig linearHashConfig;
	private List<ChainMetaData> chainMetaDatas = new ArrayList<>();

	public LinearHashFile(Tuple tuple, LinearHashConfig linearHashConfig) {
		this.tuple = tuple;
		this.linearHashConfig = linearHashConfig;
	}

	public Tuple getTuple() {
		return tuple;
	}

	public void setTuple(Tuple tuple) {
		this.tuple = tuple;
	}

	public LinearHashConfig getLinearHashConfig() {
		return linearHashConfig;
	}

	public List<ChainMetaData> getChainMetaDatas() {
		return chainMetaDatas;
	}

	public void setLinearHashConfig(LinearHashConfig linearHashConfig) {
		this.linearHashConfig = linearHashConfig;
	}

	// Utility Methods.

	/**
	 * Creates <b>m</b> empty pages with default headers. All created page Ids will
	 * be added to the directory.
	 * 
	 * @throws IOException
	 */
	public void initLinearHashWithOutPageIds() throws IOException {
		ChainMetaData chainMeataData = null;
		for (int i = 0; i < this.linearHashConfig.getCurrentM(); i++) {
			int rootPageId = LinearHashUtils.createRootPage(linearHashConfig, tuple);
			chainMeataData = new ChainMetaData(rootPageId, 1);
			chainMetaDatas.add(chainMeataData);
		}
		int directoryRootId = LinearHashUtils.createRootDirectory(chainMetaDatas);
		this.linearHashConfig.setSp(0);
		this.linearHashConfig.setDirectoryPageId(directoryRootId);
	}

	/**
	 * Reads chain page ids from the directory.
	 * 
	 * @throws IOException
	 */
	public void initLinearHashWithPageIds() throws IOException {
		int directoryPageId = this.linearHashConfig.getDirectoryPageId();
		this.chainMetaDatas = LinearHashUtils.createChainMetaDataFromDirectory(directoryPageId);
	}

	/**
	 * Inserts a new record to the page.
	 * 
	 * @param record
	 * @throws IOException
	 */
	public void insert(byte[] record) throws IOException {
		int hash = LinearHashUtils.getHashValue(record, linearHashConfig.getCurrentM());
		ChainMetaData currentChainMetaData = this.chainMetaDatas.get(hash);
		int currentPageId = currentChainMetaData.getRootPageId();
		StorageManagerClient smc = applicationConfig.getStorageManagerClient();
		byte[] curPageBuffer = smc.readPagewithoutPin(currentPageId);
		PageHeader currentPageHeader = new PageHeader(curPageBuffer);
		// If page is already full, Add a new page to current chain and add record to
		// the new page.
		if (LinearHashUtils.isPageFull(curPageBuffer)) {
			int newPageId = smc.allocatePage();
			PageHeader newPageHeader = LinearHashUtils.createNewPageHeader(newPageId,
					linearHashConfig.getPageCapacity(), this.tuple.getLength());
			newPageHeader.setHeaderValue(HEADER_ATTR.NEXT_PAGE, currentPageId);
			int currentIdx = currentPageHeader.getHeaderValue(HEADER_ATTR.PAGE_ID);
			newPageHeader.setHeaderValue(HEADER_ATTR.PAGE_ID, currentIdx + 1);
			byte[] header = newPageHeader.getHeader();
			curPageBuffer = smc.readPagewithoutPin(newPageId);
			currentPageHeader = newPageHeader;
			currentPageId = newPageId;
			currentChainMetaData.setRootPageId(currentPageId);
			currentChainMetaData.setLength(currentChainMetaData.getLength() + 1);
			// this.chainMetaDatas.set(hash, currentChainMetaData);
			System.arraycopy(header, 0, curPageBuffer, 0, header.length);
		}
		// Copy record to the buffer
		int offset = LinearHashUtils.getPageOffset(currentPageHeader);
		System.arraycopy(record, 0, curPageBuffer, offset, record.length);
		// Update Header with num of tuples.
		int numTuples = currentPageHeader.getHeaderValue(HEADER_ATTR.NUM_TUPLES);
		currentPageHeader.setHeaderValue(HEADER_ATTR.NUM_TUPLES, numTuples + 1);
		byte[] header = currentPageHeader.getHeader();
		System.arraycopy(header, 0, curPageBuffer, 0, header.length);
		smc.writePagewithoutPin(currentPageId, curPageBuffer);
		smc.flushBuffer();

		// Get average Length -- compare with the min and max
		int sp = this.linearHashConfig.getSp();
		int m = this.linearHashConfig.getCurrentM();
		boolean splitHappened = false;
		while (getAverageChainLength() > linearHashConfig.getUpperBound()) {
			splitHappened = true;
			split(sp);
			if (sp < m - 1) {
				sp++;
			} else {
				m *= 2;
				sp = 0;
			}
		}

		// If split happens, update directory and config values.
		if (splitHappened) {
			this.linearHashConfig.setCurrentM(m);
			this.linearHashConfig.setSp(sp);
			LinearHashUtils.updateDirectory(this.linearHashConfig.getDirectoryPageId(), this.chainMetaDatas);
		}

	}

	/**
	 * Finds and deletes a record with the key.
	 * 
	 * @param key
	 * @throws IOException
	 */
	public boolean remove(int key) throws IOException {
		int m = this.linearHashConfig.getCurrentM();
		int sp = this.linearHashConfig.getSp();
		int hash = key % m >= sp ? key % m : key % (2 * m);
		if (searchAndRemove(hash, key)) {
			boolean mergeHappened = false;
			double avgChainLen = getAverageChainLength();
			while (m > linearHashConfig.getInitialM() && getAverageChainLength() < linearHashConfig.getLowerBound()
					&& avgChainLen < linearHashConfig.getUpperBound()) {
				mergeHappened = true;
				if (sp == 0) {
					m /= 2;
					sp = m;
				}
				merge(sp + m - 1, sp - 1);
				sp--;
			}
			if (mergeHappened) {
				this.linearHashConfig.setCurrentM(m);
				this.linearHashConfig.setSp(sp);
				LinearHashUtils.updateDirectory(this.linearHashConfig.getDirectoryPageId(), this.chainMetaDatas);
			}
			return true;
		}

		System.out.println("Record doesn't exist with this key.");
		return false;

	}

	/**
	 * Bulk upload records.
	 * 
	 * @param records
	 * @throws IOException
	 */
	public void bulkInsert(List<byte[]> records) throws IOException {
		int m = this.linearHashConfig.getCurrentM();
		int sp = this.linearHashConfig.getSp();
		final int maxRecordsPerPage = (linearHashConfig.getPageCapacity() - PageHeader.DEFAULT_PAGE_HEADER_SIZE)
				/ (tuple.getLength());

		// Initial {hash, record[]} mapping.
		SortedMap<Integer, List<byte[]>> map = new TreeMap<>();
		for (byte[] record : records) {
			int hash = ByteUtils.byteArrayToInt(record, 0) % m;
			if (!map.containsKey(hash)) {
				map.put(hash, new ArrayList<>());
			}
			map.get(hash).add(record);
		}

		// Find Average Chain Length - function.
		ToDoubleFunction<Map<Integer, List<byte[]>>> getAvg = (chainMap) -> {
			int totalChains = chainMap.size();
			double chainLens = chainMap.values().stream()
					.mapToDouble(list -> Math.ceil(list.size() / (double) maxRecordsPerPage)).sum();
			return chainLens / totalChains;
		};

		// Split map - function.
		BiConsumer<Integer, Integer> split = (spVal, median) -> {
			List<byte[]> chainRecords = new ArrayList<>(map.get(spVal));
			int newPage = map.size();
			map.put(newPage, new ArrayList<>());
			map.get(spVal).clear();
			// Rehash records.
			for (byte[] chainRecord : chainRecords) {
				int hash = ByteUtils.byteArrayToInt(chainRecord, 0) % (2 * median);
				if (hash == spVal)
					map.get(hash).add(chainRecord);
				else
					map.get(newPage).add(chainRecord);
			}
		};

		// Update map.
		while (getAvg.applyAsDouble(map) > linearHashConfig.getUpperBound()) {
			// Split map
			split.accept(sp, m);
			if (sp < m - 1) {
				sp++;
			} else {
				m *= 2;
				sp = 0;
			}
		}

		this.linearHashConfig.setCurrentM(m);
		this.linearHashConfig.setSp(sp);

		// Finally write map to the storage and update directory.
		bulkLoadRecordsToStorage(map);
		LinearHashUtils.updateDirectory(this.linearHashConfig.getDirectoryPageId(), this.chainMetaDatas);
	}

	/**
	 * Updaate configuration file.
	 * 
	 * @param configFile
	 */
	public void updateConfigAndStorageCatalogue(String configFile) {
		LinearHashUtils.marshallConfigFile(configFile, this.linearHashConfig);
		// StorageDirectoryDocument xmlParser = applicationConfig.getXmlParser();
		// xmlParser.removeXMLNode(this.linearHashConfig.getName());
		// xmlParser.addXMLDocument(this.linearHashConfig.getName(),
		// this.linearHashConfig.getName(),
		// String.valueOf(this.linearHashConfig.getSequencePageId()),
		// String.valueOf(1));
		// try {
		// xmlParser.writeXmlFile(new
		// File(applicationConfig.getStorageUtils().xmlCatalog_File_Path),
		// xmlParser.getXMLDocument());
		// } catch (Exception e) {
		// System.out.println("Failed on updating catalog file!");
		// }

	}

	/**
	 * Search for the key and remove the record.
	 * 
	 * @param hash
	 * @param key
	 * @return
	 * @throws IOException
	 */
	private boolean searchAndRemove(int hash, int key) throws IOException {
		StorageManagerClient smc = applicationConfig.getStorageManagerClient();
		ChainMetaData curChainMetaData = chainMetaDatas.get(hash);
		int pageId = curChainMetaData.getRootPageId();
		int curPage = pageId;
		while (curPage != -1) {
			byte[] buffer = smc.readPagewithoutPin(curPage);
			PageHeader pageHeader = new PageHeader(buffer);
			int headerSize = pageHeader.getHeaderValue(HEADER_ATTR.HEADER_SIZE);
			int tupleLen = pageHeader.getHeaderValue(HEADER_ATTR.TUPLE_LENGTH);
			int numTuples = pageHeader.getHeaderValue(HEADER_ATTR.NUM_TUPLES);
			int offset = headerSize;
			for (int i = 0; i < numTuples; i++) {
				int recordId = ByteUtils.byteArrayToInt(buffer, offset);
				if (recordId == key) {
					if (numTuples == 1 && curChainMetaData.getLength() > 1) {
						// Allowed for page deallocation.
						int nextPage = pageHeader.getHeaderValue(HEADER_ATTR.NEXT_PAGE);
						curChainMetaData.setRootPageId(nextPage);
						curChainMetaData.setLength(curChainMetaData.getLength() - 1);
						smc.deallocatePage(curPage);
					} else {
						// Remove records and shift remaining content.
						System.arraycopy(buffer, offset + tupleLen, buffer, offset, (numTuples - i - 1) * tupleLen);
						// Update header.
						pageHeader.setHeaderValue(HEADER_ATTR.NUM_TUPLES, numTuples - 1);
						byte[] header = pageHeader.getHeader();
						System.arraycopy(header, 0, buffer, 0, header.length);
						smc.writePagewithoutPin(curPage, buffer);
					}
					return true;
				}
				offset += tupleLen;
			}
			curPage = pageHeader.getHeaderValue(HEADER_ATTR.NEXT_PAGE);

		}
		return false;
	}

	/**
	 * Split current chain.
	 * 
	 * @param sp
	 * @throws IOException
	 */
	private void split(int sp) throws IOException {
		StorageManagerClient smc = applicationConfig.getStorageManagerClient();
		int bufferPageSize = applicationConfig.getStorageUtils().getStorageConfig().getPageSize(); // in KB
		byte[][] buffer = new byte[2][bufferPageSize * 1024];
		int mPage = LinearHashUtils.createRootPage(linearHashConfig, tuple);
		int mChainLen = 1;
		int twoMPage = LinearHashUtils.createRootPage(linearHashConfig, tuple);
		int twoMChainLen = 1;
		int curMPage = mPage, curTwoMPage = twoMPage;
		int m = linearHashConfig.getCurrentM();
		int currentPageId = chainMetaDatas.get(sp).getRootPageId();
		ChainIterator chainItr = new ChainIterator(currentPageId);
		buffer[0] = smc.readPagewithoutPin(curMPage);
		buffer[1] = smc.readPagewithoutPin(curTwoMPage);
		PageHeader curMPageHeader = new PageHeader(buffer[0]);
		PageHeader curTwoMPageHeader = new PageHeader(buffer[1]);
		while (chainItr.hasNext()) {
			byte[] record = chainItr.next();
			int hash = LinearHashUtils.getHashValue(record, 2 * m);
			if (hash == sp) {
				// Add record to page m
				if (LinearHashUtils.isPageFull(buffer[0])) {
					smc.writePagewithoutPin(curMPage, buffer[0]);
					// Add a new page to current chain and add record to the new page.
					int newPageId = smc.allocatePage();
					PageHeader newPageHeader = LinearHashUtils.createNewPageHeader(newPageId,
							linearHashConfig.getPageCapacity(), this.tuple.getLength());
					newPageHeader.setHeaderValue(HEADER_ATTR.NEXT_PAGE, curMPage);
					int currentIdx = curMPageHeader.getHeaderValue(HEADER_ATTR.PAGE_ID);
					newPageHeader.setHeaderValue(HEADER_ATTR.PAGE_ID, currentIdx + 1);
					byte[] header = newPageHeader.getHeader();
					buffer[0] = smc.readPagewithoutPin(newPageId);
					curMPageHeader = newPageHeader;
					curMPage = newPageId;
					System.arraycopy(header, 0, buffer[0], 0, header.length);
					mChainLen++;
				}
				int mOffset = LinearHashUtils.getPageOffset(curMPageHeader);
				int mNumTuples = curMPageHeader.getHeaderValue(HEADER_ATTR.NUM_TUPLES);
				curMPageHeader.setHeaderValue(HEADER_ATTR.NUM_TUPLES, mNumTuples + 1);
				byte[] header = curMPageHeader.getHeader();
				System.arraycopy(record, 0, buffer[0], mOffset, record.length);
				System.arraycopy(header, 0, buffer[0], 0, header.length);

			} else {
				// Add record to page m+sp-1.
				if (LinearHashUtils.isPageFull(buffer[1])) {
					smc.writePagewithoutPin(curTwoMPage, buffer[1]);
					// Add a new page to current chain and add record to the new page.
					int newPageId = smc.allocatePage();
					PageHeader newPageHeader = LinearHashUtils.createNewPageHeader(newPageId,
							linearHashConfig.getPageCapacity(), this.tuple.getLength());
					newPageHeader.setHeaderValue(HEADER_ATTR.NEXT_PAGE, curMPage);
					int currentIdx = curTwoMPageHeader.getHeaderValue(HEADER_ATTR.PAGE_ID);
					newPageHeader.setHeaderValue(HEADER_ATTR.PAGE_ID, currentIdx + 1);
					byte[] header = newPageHeader.getHeader();
					buffer[1] = smc.readPagewithoutPin(newPageId);
					curTwoMPageHeader = newPageHeader;
					curTwoMPage = newPageId;
					System.arraycopy(header, 0, buffer[1], 0, header.length);
					twoMPage++;
				}
				int twoMOffset = LinearHashUtils.getPageOffset(curTwoMPageHeader);
				int twoMNumTuples = curTwoMPageHeader.getHeaderValue(HEADER_ATTR.NUM_TUPLES);
				curTwoMPageHeader.setHeaderValue(HEADER_ATTR.NUM_TUPLES, twoMNumTuples + 1);
				byte[] header = curTwoMPageHeader.getHeader();
				System.arraycopy(record, 0, buffer[1], twoMOffset, record.length);
				System.arraycopy(header, 0, buffer[1], 0, header.length);
			}
		}
		// Deallocate all pages in sp chain.
		for (int id : chainItr.getPageIds()) {
			smc.deallocatePage(id);
		}
		smc.writePagewithoutPin(curMPage, buffer[0]);
		smc.writePagewithoutPin(curTwoMPage, buffer[1]);
		smc.flushBuffer();
		smc.writeBitMap();
		// Update meta data info.s
		this.chainMetaDatas.set(sp, new ChainMetaData(curMPage, mChainLen));
		this.chainMetaDatas.add(new ChainMetaData(curTwoMPage, twoMChainLen));

	}

	/**
	 * Merges source chain to dest chain.
	 * 
	 * @param sourceId
	 * @param destId
	 * @throws IOException
	 */
	private void merge(int sourceId, int destId) throws IOException {
		int sourcePageId = chainMetaDatas.get(sourceId).getRootPageId();
		int destPageId = chainMetaDatas.get(destId).getRootPageId();
		StorageManagerClient smc = applicationConfig.getStorageManagerClient();
		ChainIterator chainIterator = new ChainIterator(sourcePageId);
		byte[] destBuffer = smc.readPagewithoutPin(destPageId);
		PageHeader destPageHeader = new PageHeader(destBuffer);
		int destChainLen = chainMetaDatas.get(destId).getLength();
		// Iterate on source records.
		while (chainIterator.hasNext()) {
			byte[] record = chainIterator.next();
			if (LinearHashUtils.isPageFull(destBuffer)) {
				smc.writePagewithoutPin(destPageId, destBuffer);
				// Add a new page to current chain and add record to the new page.
				int newPageId = smc.allocatePage();
				PageHeader newPageHeader = LinearHashUtils.createNewPageHeader(newPageId,
						linearHashConfig.getPageCapacity(), this.tuple.getLength());
				newPageHeader.setHeaderValue(HEADER_ATTR.NEXT_PAGE, destPageId);
				int currentIdx = destPageHeader.getHeaderValue(HEADER_ATTR.PAGE_ID);
				newPageHeader.setHeaderValue(HEADER_ATTR.PAGE_ID, currentIdx + 1);
				byte[] header = newPageHeader.getHeader();
				destBuffer = smc.readPagewithoutPin(newPageId);
				destPageHeader = newPageHeader;
				destPageId = newPageId;
				destChainLen++;
				System.arraycopy(header, 0, destBuffer, 0, header.length);
			}
			int offset = LinearHashUtils.getPageOffset(destPageHeader);
			System.arraycopy(record, 0, destBuffer, offset, record.length);
			// Update Header with num of tuples.
			int numTuples = destPageHeader.getHeaderValue(HEADER_ATTR.NUM_TUPLES);
			destPageHeader.setHeaderValue(HEADER_ATTR.NUM_TUPLES, numTuples + 1);
			byte[] header = destPageHeader.getHeader();
			System.arraycopy(header, 0, destBuffer, 0, header.length);
		}
		// Deallocate source pages.
		for (Integer id : chainIterator.getPageIds()) {
			smc.deallocatePage(id);
		}
		smc.writePagewithoutPin(destPageId, destBuffer);
		smc.flushBuffer();
		smc.writeBitMap();

		// Update directory
		this.chainMetaDatas.set(destPageId, new ChainMetaData(destPageId, destChainLen));

	}

	/**
	 * Bulk write map records to the storage.
	 * 
	 * @param map
	 * @throws IOException
	 */
	private void bulkLoadRecordsToStorage(SortedMap<Integer, List<byte[]>> map) throws IOException {
		for (Entry<Integer, List<byte[]>> entry : map.entrySet()) {
			int hash = entry.getKey();
			List<byte[]> records = entry.getValue();
			ChainMetaData cmd = null;
			if (hash < chainMetaDatas.size()) {
				cmd = chainMetaDatas.get(hash);
			} else {
				int rootPageId = LinearHashUtils.createRootPage(linearHashConfig, tuple);
				cmd = new ChainMetaData(rootPageId, 1);
				chainMetaDatas.add(cmd);
			}
			int totalPages = addRecordsToEmptyChain(cmd.getRootPageId(), records);
			cmd.setLength(totalPages);
		}
	}

	/**
	 * Bulk load records to a single chain.
	 * 
	 * @param chainRootId
	 * @param records
	 * @return
	 * @throws IOException
	 */
	private int addRecordsToEmptyChain(int chainRootId, List<byte[]> records) throws IOException {
		final int maxRecordsPerPage = (linearHashConfig.getPageCapacity() - PageHeader.DEFAULT_PAGE_HEADER_SIZE)
				/ (tuple.getLength());
		StorageManagerClient smc = applicationConfig.getStorageManagerClient();
		final int totalPages = (int) Math.ceil((double) records.size() / maxRecordsPerPage);
		int curPageId = chainRootId;
		byte[] curPageBuffer = smc.readPagewithoutPin(curPageId);
		PageHeader currentPageHeader = new PageHeader(curPageBuffer);
		int offset = LinearHashUtils.getPageOffset(currentPageHeader);
		int recordsAdded = 0;
		for (byte[] record : records) {
			if (recordsAdded >= maxRecordsPerPage) {
				// Write to page
				currentPageHeader.setHeaderValue(HEADER_ATTR.NUM_TUPLES, recordsAdded - 1);
				byte[] header = currentPageHeader.getHeader();
				System.arraycopy(header, 0, curPageBuffer, 0, header.length);
				smc.writePagewithoutPin(curPageId, curPageBuffer);

				// Create new page
				int newPageId = smc.allocatePage();
				PageHeader newPageHeader = LinearHashUtils.createNewPageHeader(newPageId,
						linearHashConfig.getPageCapacity(), this.tuple.getLength());
				newPageHeader.setHeaderValue(HEADER_ATTR.NEXT_PAGE, curPageId);
				header = newPageHeader.getHeader();
				curPageBuffer = smc.readPagewithoutPin(newPageId);
				curPageId = newPageId;
				currentPageHeader = newPageHeader;
				offset = LinearHashUtils.getPageOffset(currentPageHeader);
				System.arraycopy(header, 0, curPageBuffer, 0, header.length);
				recordsAdded = 0;
			}

			System.arraycopy(record, 0, curPageBuffer, offset, record.length);
			recordsAdded++;
			offset += tuple.getLength();
		}
		currentPageHeader.setHeaderValue(HEADER_ATTR.NUM_TUPLES, recordsAdded - 1);
		byte[] header = currentPageHeader.getHeader();
		System.arraycopy(header, 0, curPageBuffer, 0, header.length);
		smc.writePagewithoutPin(curPageId, curPageBuffer);
		smc.flushBuffer();
		return totalPages;
	}

	/**
	 * Calculate average chain length from meta-data.
	 * 
	 * @return
	 */
	private double getAverageChainLength() {
		return this.chainMetaDatas.stream().mapToInt(ChainMetaData::getLength).average().orElse(0.0);
	}

}
