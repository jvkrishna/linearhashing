package cyclients.linearhashing.adapter;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.xml.bind.JAXBException;

import cyclients.linearhashing.LinearHashFile;
import cyclients.linearhashing.LinearHashFileFactory;
import cyclients.linearhashing.Tuple;
import cyclients.linearhashing.TupleAttribute;
import cyclients.linearhashing.util.ApplicationConfig;
import cyclients.linearhashing.util.ByteUtils;
import cyclients.linearhashing.util.LinearHashUtils;
import cysystem.clientsmanager.ClientsFactory;
import cysystem.clientsmanager.CyGUI;
import cysystem.diwGUI.gui.DBGui;
import storagemanager.StorageUtils;

/**
 * Adapter class for CyDIW commands.
 * 
 * @author Vamsi Krishna J
 * @author Abdullah
 * @author Shaiqur Rahman
 *
 */
public class LinearHashingAdapter extends ClientsFactory {

	private ApplicationConfig applicationConfig = ApplicationConfig.getInstance();
	private StorageUtils storageUtils;

	/**
	 * Overrides initializer on clients factory.
	 */
	public void initialize(CyGUI gui, int clientID) {
		this.dbgui = gui;
		this.storageUtils = ((DBGui) dbgui).getStorageUtils();
	}

	/**
	 * Entry point for commands.
	 */
	@Override
	public void execute(int clientId, String command) {
		if (this.dbgui == null) {
			System.out.println("Error! The client parser is not initialized properly."
					+ " The handle to CyDIW GUI is not initialized.");
			return;
		}
		// Initialize the configuration with storage utils.
		applicationConfig.initialize(storageUtils);
		String[] commands = command.trim().split("\\s+");
		Arrays.stream(commands).forEach(c -> c = c.trim());
		String function = commands[0].toLowerCase();
		switch (function) {
		case "list":
			listCommands();
			break;
		case "linearhashcreateempty":
			createEmptyLinearHashFile(commands);
			break;
		case "linearhashinsert":
			insertTuple(commands);
			break;
		case "linearhashdelete":
			deleteTuple(commands);
			break;
		case "linearhashbulkload":
			bulkLoad(commands);
			break;
		case "linearhashpreparesorteddata":
			prepareSortedData(commands);
			break;
		case "linearhashprintstorageinfo":
			printLinearHashStorageInfo(commands);
			break;
		default:
			dbgui.addConsoleMessage("Wrong use of commands, type 'list commands' for reference");
			break;
		}

	}

	/**
	 * Prints commands supported by Linear Hashing
	 */
	private void listCommands() {
		dbgui.addOutputPlainText("$ASVLH Commands List:");
		dbgui.addOutputPlainText("$ASVLH:> list commands;");
		dbgui.addOutputPlainText("$ASVLH:> LinearHashCreateEmpty <LinearHashConfigXMLFile> <TupleConfigXmlFile>;");
		dbgui.addOutputPlainText(
				"$ASVLH:> LinearHashInsert <LinearHashConfigXMLFile> <TupleConfigXmlFile> [OneTuple];");
		dbgui.addOutputPlainText("$ASVLH:> LinearHashDelete <LinearHashConfigXMLFile> <TupleTxtFile> <TupleKey>;");
		dbgui.addOutputPlainText("$ASVLH:> LinearHashPrepareSortedData <TupleConfigXmlFile> <TupleTxtFile>;");
		dbgui.addOutputPlainText(
				"$ASVLH:> LinearHashBulkLoad <LinearHashConfigXMLFile> <TupleConfigXmlFile> <TupleTxtFile>;");
		dbgui.addOutputPlainText(
				"$ASVLH:> LinearHashPrintStorageInfo <LinearHashConfigXMLFile> <TupleConfigXmlFile> <ResultFile>;");
	}

	/**
	 * Prepares empty linear hash file with <b>m</b> chains.
	 * 
	 * @param commands
	 */
	private void createEmptyLinearHashFile(String[] commands) {
		dbgui.addOutputPlainText("$ASVLH:> LinearHashCreateEmpty <LinearHashConfigXmlFile> <TupleConfigXmlFile>;");
		if (commands.length != 3) {
			dbgui.addOutputPlainText("Wrong command parameters, type 'list commands' for reference");
			return;
		}
		String lhashConfigFile = dbgui.getVariableValue(commands[1].substring(2));
		String tupleConfigFile = dbgui.getVariableValue(commands[2].substring(2));
		LinearHashFile linearHashFile;
		try {
			linearHashFile = LinearHashFileFactory.buildWithOutDirectory(lhashConfigFile, tupleConfigFile);
			linearHashFile.updateConfigAndStorageCatalogue(lhashConfigFile);
			dbgui.addOutputPlainText("Successfully created new directory with "
					+ linearHashFile.getLinearHashConfig().getCurrentM() + " pages. Directory page Id: "
					+ linearHashFile.getLinearHashConfig().getDirectoryPageId());
		} catch (IOException | JAXBException e) {
			dbgui.addOutputPlainText("Linear hash initialization failed.");
			e.printStackTrace();
		}
	}

	/**
	 * Inserts record to the hashed chain.
	 * 
	 * @param commands
	 */
	private void insertTuple(String[] commands) {
		dbgui.addOutputPlainText(
				"$ASVLH:> LinearHashInsert <LinearHashConfigXmlFile> <TupleConfigXmlFile> [OneTuple];");
		if (commands.length != 4) {
			dbgui.addOutputPlainText("Wrong command parameters, type 'list commands' for reference");
			return;
		}
		String lhashConfigFile = dbgui.getVariableValue(commands[1].substring(2));
		String tupleConfigFile = dbgui.getVariableValue(commands[2].substring(2));
		String tupleData = commands[3];
		LinearHashFile linearHashFile = null;
		try {
			linearHashFile = LinearHashFileFactory.buildWithDirectory(lhashConfigFile, tupleConfigFile);
		} catch (IOException | JAXBException e) {
			dbgui.addOutputPlainText("Linear hash initialization failed.");
			e.printStackTrace();
			return;
		}
		Tuple tupleDefinition = new Tuple(tupleConfigFile);
		String currentContent = tupleData.substring(1, tupleData.length() - 1);
		String[] contentArray = currentContent.split(",");
		if (tupleDefinition.getAttributeNum() != contentArray.length) {
			dbgui.addOutputPlainText("Tuple format error!");
			return;
		}
		// currentLine -> buffer
		byte[] tuple = new byte[tupleDefinition.getLength()];
		int offset = 0;
		int count = 0;
		for (TupleAttribute tupleAttribute : tupleDefinition.getTupleAttributes()) {
			String attrType = tupleAttribute.getType();
			int attrLen = tupleAttribute.getLength();
			if (attrType.equals("Integer") && attrLen == 4) {
				// if it is an integer
				ByteUtils.intToByteArray(Integer.parseInt(contentArray[count].trim()), tuple, offset);
			} else if (attrType.equals("String")) {
				// if it is a string
				ByteUtils.stringToByteArray(contentArray[count], tuple, offset, offset + attrLen);
			}
			count++;
			offset += attrLen;
		}
		try {
			linearHashFile.insert(tuple);
			dbgui.addOutputPlainText("Successfully inserted the record.!");
			linearHashFile.updateConfigAndStorageCatalogue(lhashConfigFile);
		} catch (IOException e) {
			dbgui.addOutputPlainText("Record insertion failed.");
			e.printStackTrace();
		}

	}

	/**
	 * Deletes record from the page.
	 * 
	 * @param commands
	 */
	private void deleteTuple(String[] commands) {
		dbgui.addOutputPlainText(
				"$ASVLH:> LinearHashCreateEmpty <LinearHashConfigXmlFile> <TupleConfigXmlFile> [TupleKey];");
		if (commands.length != 4) {
			dbgui.addOutputPlainText("Wrong command parameters, type 'list commands' for reference");
			return;
		}
		String lhashConfigFile = dbgui.getVariableValue(commands[1].substring(2));
		String tupleConfigFile = dbgui.getVariableValue(commands[2].substring(2));
		int key = Integer.parseInt(commands[3]);
		try {
			LinearHashFile linearHashFile = LinearHashFileFactory.buildWithDirectory(lhashConfigFile, tupleConfigFile);
			boolean removed = linearHashFile.remove(key);
			dbgui.addOutputPlainText(removed ? "Successfully deleted the record!" : "Record doesn't exist.");
			linearHashFile.updateConfigAndStorageCatalogue(lhashConfigFile);
		} catch (IOException | JAXBException e) {
			System.out.println("Record deletion failed.");
			e.printStackTrace();
		}
	}

	/**
	 * Generates sorted data for insertion.
	 * 
	 * @param commands
	 */
	private void prepareSortedData(String[] commands) {
		dbgui.addOutputPlainText("$ASVLH:> LinearHashPrepareSortedData <TupleConfigXmlFile> <TupleTxtFile>;");

		if (commands.length != 3) {
			dbgui.addOutputPlainText("Wrong command parameters, type 'list commands' for reference");
			return;
		}
		String tupleConfigFile = dbgui.getVariableValue(commands[1].trim().substring(2));
		String tupleDataFile = dbgui.getVariableValue(commands[2].trim().substring(2));
		try {
			LinearHashUtils.prepareSortedData(tupleConfigFile, tupleDataFile);
			dbgui.addOutputPlainText("Successfully generated sorted data into the storage.!");
		} catch (Exception e) {
			dbgui.addOutputPlainText("IO Exception during preparing sorted data for linear hash");
		}
	}

	/**
	 * Bulk loads records from the file.
	 * 
	 * @param commands
	 */
	private void bulkLoad(String[] commands) {
		dbgui.addOutputPlainText(
				"$ASVLH:> LinearHashBulkLoad <LinearHashConfigXmlFile> <TupleConfigXmlFile> <TupleTxtFile>;");

		if (commands.length != 4) {
			dbgui.addOutputPlainText("Lack of command parameters, type 'list commands' for reference");
			return;
		}
		String lhashConfigFile = dbgui.getVariableValue(commands[1].substring(2));
		String tupleConfigFile = dbgui.getVariableValue(commands[2].substring(2));
		String tupleDataFile = dbgui.getVariableValue(commands[3].trim().substring(2));
		try {
			LinearHashFile linearHashFile = LinearHashFileFactory.buildWithDirectory(lhashConfigFile, tupleConfigFile);
			Tuple tuple = new Tuple(tupleConfigFile);
			List<byte[]> records = LinearHashUtils.readRecords(tupleDataFile, tuple);
			linearHashFile.bulkInsert(records);
			linearHashFile.updateConfigAndStorageCatalogue(lhashConfigFile);
			dbgui.addOutputPlainText("Successfully bulkloaded sorted data into storage!");
		} catch (Exception e) {
			e.printStackTrace();
			dbgui.addOutputPlainText("Exception during bulkload.");
		}
	}

	/**
	 * Prints the storage meta data into a file.
	 * 
	 * @param commands
	 */
	private void printLinearHashStorageInfo(String[] commands) {
		dbgui.addOutputPlainText(
				"$ASVLH:> LinearHashBulkLoad <LinearHashConfigXmlFile> <TupleConfigXmlFile> <ResultTxtFile>;");
		if (commands.length != 4) {
			dbgui.addOutputPlainText("Lack of command parameters, type 'list commands' for reference");
			return;
		}
		String lhashConfigFile = dbgui.getVariableValue(commands[1].substring(2));
		String tupleConfigFile = dbgui.getVariableValue(commands[2].substring(2));
		String resultsFile = dbgui.getVariableValue(commands[3].trim().substring(2));
		try {
			LinearHashFile linearHashFile = LinearHashFileFactory.buildWithDirectory(lhashConfigFile, tupleConfigFile);
			LinearHashUtils.printStorageInfoToFile(resultsFile, linearHashFile);
			dbgui.addOutputPlainText("Successfully generated storage metadata.!");
		} catch (IOException | JAXBException e) {
			dbgui.addOutputPlainText("Failed to generate storage metadata.!");
			e.printStackTrace();
		}
	}

}
