package cyclients.linearhashing;

/**
 * POJO for holding page pointers and length.
 * 
 * @author Vamsi Krishna J
 *
 */
public class ChainMetaData {

	private int rootPageId;
	private int length;

	public ChainMetaData(int pageId, int length) {
		this.rootPageId = pageId;
		this.length = length;
	}

	public int getRootPageId() {
		return rootPageId;
	}

	public void setRootPageId(int rootPageId) {
		this.rootPageId = rootPageId;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

}
