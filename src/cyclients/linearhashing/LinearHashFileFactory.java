package cyclients.linearhashing;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import cyclients.linearhashing.entities.LinearHashConfig;
import cyclients.linearhashing.util.LinearHashUtils;

/**
 * Factory class for creating {@link LinearHashFile}.
 * 
 * @author Vamsi Krishna J
 *
 */
public class LinearHashFileFactory {

	/**
	 * Creates and initializes {@link LinearHashFile} from empty directory.
	 * 
	 * @param lhashConfigFile
	 * @param tupleConfigFile
	 * @return
	 * @throws IOException
	 * @throws JAXBException
	 */
	public static LinearHashFile buildWithOutDirectory(String lhashConfigFile, String tupleConfigFile)
			throws IOException, JAXBException {
		Tuple tuple = new Tuple(tupleConfigFile);
		LinearHashConfig linearHashConfig;
		linearHashConfig = LinearHashUtils.unmarshallConfigFile(lhashConfigFile);
		LinearHashFile linearHashFile = new LinearHashFile(tuple, linearHashConfig);
		linearHashFile.initLinearHashWithOutPageIds();
		return linearHashFile;

	}

	/**
	 * Creates and initializes {@link LinearHashFile} from existing directory.
	 * 
	 * @param lhashConfigFile
	 * @param tupleConfigFile
	 * @return
	 * @throws IOException
	 * @throws JAXBException
	 */
	public static LinearHashFile buildWithDirectory(String lhashConfigFile, String tupleConfigFile) throws IOException, JAXBException {
		Tuple tuple = new Tuple(tupleConfigFile);
		LinearHashConfig linearHashConfig;
		linearHashConfig = LinearHashUtils.unmarshallConfigFile(lhashConfigFile);
		LinearHashFile linearHashFile = new LinearHashFile(tuple, linearHashConfig);
		linearHashFile.initLinearHashWithPageIds();
		return linearHashFile;
	}
}
