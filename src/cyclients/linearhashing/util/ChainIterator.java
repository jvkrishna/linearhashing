package cyclients.linearhashing.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import cyclients.linearhashing.util.PageHeader.HEADER_ATTR;
import storagemanager.StorageManagerClient;

/**
 * Iterator for reading reading records from chain of pages.
 * 
 * @author krishnaj
 *
 */
public class ChainIterator implements Iterator<byte[]> {

	private StorageManagerClient smc = ApplicationConfig.getInstance().getStorageManagerClient();
	private int curPageId;
	private int nextPageId;
	private int offset;
	private int recordLen;
	private int totalRecords;
	private int recordsRemain;
	private byte[] buffer;
	private List<Integer> pageIds = new ArrayList<>();

	public ChainIterator(int rootPageId) throws IOException {
		updateParams(rootPageId);
	}

	private void updateParams(int rootPageId) throws IOException {
		pageIds.add(rootPageId);
		this.curPageId = rootPageId;
		this.buffer = smc.readPagewithoutPin(curPageId);
		PageHeader pageHeader = new PageHeader(buffer);
		this.nextPageId = pageHeader.getHeaderValue(HEADER_ATTR.NEXT_PAGE);
		this.offset = pageHeader.getHeaderValue(HEADER_ATTR.HEADER_SIZE);
		this.recordLen = pageHeader.getHeaderValue(HEADER_ATTR.TUPLE_LENGTH);
		this.totalRecords = pageHeader.getHeaderValue(HEADER_ATTR.NUM_TUPLES);
		this.recordsRemain = totalRecords;
	}

	@Override
	public boolean hasNext() {
		return nextPageId != -1 || recordsRemain != 0;
	}

	@Override
	public byte[] next() {
		if (!hasNext()) {
			throw new NoSuchElementException();
		}
		if (recordsRemain == 0) {
			try {
				updateParams(this.nextPageId);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		byte[] record = ByteUtils.getRecord(buffer, offset, recordLen);
		this.recordsRemain--;
		this.offset += this.recordLen;
		return record;
	}

	/**
	 * Returns the list of page Ids read so far.
	 * 
	 * @return
	 */
	public List<Integer> getPageIds() {
		return pageIds;
	}

}
