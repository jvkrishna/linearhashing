package cyclients.linearhashing.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Stream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import cyclients.linearhashing.ChainMetaData;
import cyclients.linearhashing.DirectoryPage;
import cyclients.linearhashing.DirectoryPage.DirectoryPageHeader;
import cyclients.linearhashing.LinearHashFile;
import cyclients.linearhashing.Tuple;
import cyclients.linearhashing.TupleAttribute;
import cyclients.linearhashing.entities.LinearHashConfig;
import cyclients.linearhashing.util.PageHeader.HEADER_ATTR;
import storagemanager.StorageManagerClient;

/**
 * Utility methods for handling the linear hash functionalities.
 * 
 * @author Vamsi Krishna J
 *
 */
public class LinearHashUtils {

	private static ApplicationConfig applicationConfig = ApplicationConfig.getInstance();
	private static StorageManagerClient smc = applicationConfig.getStorageManagerClient();

	/**
	 * 
	 * Unmarshalls config xml file to object.
	 * 
	 * @param configFile
	 * @return
	 * @throws JAXBException
	 */
	public static LinearHashConfig unmarshallConfigFile(String configFile) throws JAXBException {
		return unmarshallObject(configFile, LinearHashConfig.class);
	}

	/**
	 * Marshalls object to XML and writes to file.
	 * 
	 * @param configFile
	 * @param linearConfig
	 */
	public static void marshallConfigFile(String configFile, LinearHashConfig linearConfig) {
		try {
			marshallObject(configFile, linearConfig);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Creates new page header with default values.
	 * 
	 * @param pageId
	 * @param pageSize
	 * @param tupleLen
	 * @return
	 */
	public static PageHeader createNewPageHeader(int pageId, int pageSize, int tupleLen) {
		PageHeader pageHeader = new PageHeader();
		pageHeader.setHeaderValue(HEADER_ATTR.HEADER_SIZE, PageHeader.DEFAULT_PAGE_HEADER_SIZE);
		pageHeader.setHeaderValue(HEADER_ATTR.CURRENT_PAGE, pageId);
		pageHeader.setHeaderValue(HEADER_ATTR.NEXT_PAGE, -1);
		pageHeader.setHeaderValue(HEADER_ATTR.PAGE_TYPE, 0);
		pageHeader.setHeaderValue(HEADER_ATTR.TUPLE_LENGTH, tupleLen);
		pageHeader.setHeaderValue(HEADER_ATTR.NUM_TUPLES, 0);
		pageHeader.setHeaderValue(HEADER_ATTR.PAGE_SIZE, pageSize);
		pageHeader.setHeaderValue(HEADER_ATTR.PAGE_ID, 1);
		return pageHeader;
	}

	/**
	 * Creates new chain with a root page and updates header.
	 * 
	 * @param linearHashConfig
	 * @param tuple
	 * @return
	 * @throws IOException
	 */
	public static int createRootPage(LinearHashConfig linearHashConfig, Tuple tuple) throws IOException {
		int rootPageId = smc.allocatePage();
		PageHeader pageHeader = createNewPageHeader(rootPageId, linearHashConfig.getPageCapacity(), tuple.getLength());
		byte[] header = pageHeader.getHeader();
		byte[] buffer = smc.readPagewithoutPin(rootPageId);
		System.arraycopy(header, 0, buffer, 0, header.length);
		smc.writePagewithoutPin(rootPageId, buffer);
		smc.flushBuffer();
		return rootPageId;
	}

	/**
	 * Checks the storage availability for the current page.
	 * 
	 * @param buffer
	 * @return
	 */
	public static boolean isPageFull(byte[] buffer) {
		PageHeader pageHeader = new PageHeader(buffer);
		int pageType = pageHeader.getHeaderValue(HEADER_ATTR.PAGE_TYPE);
		int tupleLength = pageHeader.getHeaderValue(HEADER_ATTR.TUPLE_LENGTH);
		int tupleNum = pageHeader.getHeaderValue(HEADER_ATTR.NUM_TUPLES);
		int pageHeaderSize = pageHeader.getHeaderValue(HEADER_ATTR.HEADER_SIZE);
		int pageSize = Math.min(smc.getXmlSto().getPageSize() * 1024, pageHeader.getHeaderValue(HEADER_ATTR.PAGE_SIZE));

		return pageHeaderSize + (tupleNum + 1) * tupleLength + (tupleNum + 2) * pageType > pageSize;

	}

	/**
	 * Returns the offset to add new data.
	 * 
	 * @param pageHeader
	 * @return
	 */
	public static int getPageOffset(PageHeader pageHeader) {
		int tupleLength = pageHeader.getHeaderValue(HEADER_ATTR.TUPLE_LENGTH);
		int tupleNum = pageHeader.getHeaderValue(HEADER_ATTR.NUM_TUPLES);
		int pageHeaderSize = pageHeader.getHeaderValue(HEADER_ATTR.HEADER_SIZE);
		return pageHeaderSize + (tupleNum * tupleLength);
	}

	/**
	 * Returns the hash value for the record. Assumes that key is located at zero
	 * index.
	 * 
	 * @param record
	 * @param m
	 * @return
	 */
	public static int getHashValue(byte[] record, int m) {
		int keyValue = ByteUtils.byteArrayToInt(record, 0);
		return keyValue % m;
	}

	/**
	 * Create new empty directory and writes current chain metadata into that.
	 * 
	 * @param chainMeataDataList
	 * @return
	 * @throws IOException
	 */
	public static int createRootDirectory(List<ChainMetaData> chainMeataDataList) throws IOException {
		int rootPageId = smc.allocatePage();
		updateDirectory(rootPageId, chainMeataDataList);
		return rootPageId;
	}

	/**
	 * Overwrites the directory data with the input chain metadata.
	 * 
	 * @param rootPageId
	 * @param chainMeataDataList
	 * @return
	 * @throws IOException
	 */
	public static boolean updateDirectory(int rootPageId, List<ChainMetaData> chainMeataDataList) throws IOException {
		byte[] buffer = smc.readPagewithoutPin(rootPageId);
		int curPage = rootPageId;
		int offset = DirectoryPage.DEFAULT_HEADER_SIZE;
		int pageSize = DirectoryPage.DEFAUL_PAGE_SIZE;
		int recordSize = DirectoryPage.DEFAULT_RECORD_SIZE;
		updateDirectoryPageHeader(buffer, curPage, -1);
		for (ChainMetaData cmd : chainMeataDataList) {
			if (offset + recordSize > pageSize) {
				int nextPageId = DirectoryPage.getHeader(buffer, DirectoryPageHeader.NEXT_PAGE);
				if (nextPageId == -1) {
					nextPageId = smc.allocatePage();
					DirectoryPage.setHeader(buffer, DirectoryPageHeader.NEXT_PAGE, nextPageId);
				}
				smc.writePagewithoutPin(curPage, buffer);
				buffer = smc.readPagewithoutPin(nextPageId);
				curPage = nextPageId;
				updateDirectoryPageHeader(buffer, curPage, -1);
				offset = DirectoryPage.DEFAULT_HEADER_SIZE;
			}
			ByteUtils.intToByteArray(cmd.getRootPageId(), buffer, offset);
			ByteUtils.intToByteArray(cmd.getLength(), buffer, offset + 4);
			offset += recordSize;
		}
		smc.writePagewithoutPin(curPage, buffer);
		smc.flushBuffer();
		return true;
	}

	/**
	 * Updates directory header details.
	 * 
	 * @param buffer
	 * @param curPage
	 * @param nextPage
	 */
	public static void updateDirectoryPageHeader(byte[] buffer, int curPage, int nextPage) {
		DirectoryPage.setHeader(buffer, DirectoryPageHeader.HEADER_SIZE, DirectoryPage.DEFAULT_HEADER_SIZE);
		DirectoryPage.setHeader(buffer, DirectoryPageHeader.CURRENT_PAGE, curPage);
		DirectoryPage.setHeader(buffer, DirectoryPageHeader.NEXT_PAGE, nextPage);
		DirectoryPage.setHeader(buffer, DirectoryPageHeader.PAGE_SIZE, DirectoryPage.DEFAUL_PAGE_SIZE);
	}

	/**
	 * Reads the directory and generates {@link ChainMetaData} objects.
	 * 
	 * @param rootPageId
	 * @return
	 * @throws IOException
	 */
	public static List<ChainMetaData> createChainMetaDataFromDirectory(int rootPageId) throws IOException {
		List<ChainMetaData> chainMetaDataList = new ArrayList<>();
		byte[] buffer;
		int curPage = rootPageId;
		ChainMetaData cmd = null;
		while (curPage != -1) {
			buffer = smc.readPagewithoutPin(curPage);
			int offset = DirectoryPage.DEFAULT_HEADER_SIZE;
			int pageSize = DirectoryPage.DEFAUL_PAGE_SIZE;
			int recordSize = DirectoryPage.DEFAULT_RECORD_SIZE;
			while (offset + recordSize < pageSize) {
				int chainPageId = ByteUtils.byteArrayToInt(buffer, offset);
				int chainLen = ByteUtils.byteArrayToInt(buffer, offset + 4);
				if (chainPageId == 0 || chainLen == 0)
					break;
				cmd = new ChainMetaData(chainPageId, chainLen);
				chainMetaDataList.add(cmd);
				offset += recordSize;
			}
			curPage = DirectoryPage.getHeader(buffer, DirectoryPageHeader.NEXT_PAGE);
		}
		return chainMetaDataList;
	}

	/**
	 * Generates sorted data and writes into file.
	 * 
	 * @param tupleConfigXmlFile
	 * @param tupleTxtFile
	 * @throws FileNotFoundException
	 */
	public static void prepareSortedData(String tupleConfigXmlFile, String tupleTxtFile) throws FileNotFoundException {
		final int dataSize = 10_000;
		Tuple tuple = new Tuple(tupleConfigXmlFile);
		List<TupleAttribute> attributes = tuple.getTupleAttributes();
		PrintWriter writer = new PrintWriter(tupleTxtFile);

		for (int ii = 0; ii < dataSize; ii++) {
			StringJoiner joiner = new StringJoiner(",");
			for (TupleAttribute attribute : attributes) {
				if (attribute.getType().equals("Integer")) {
					joiner.add(String.valueOf(ii));
				} else if (attribute.getType().equals("String")) {
					joiner.add(String.format("%05d", ii));
				}
			}
			String joined = "[" + joiner.toString() + "]";
			writer.println(joined);
		}
		writer.close();
	}

	/**
	 * Reads record data from file and generates byte data.
	 * 
	 * @param filePath
	 * @param tuple
	 * @return
	 * @throws IOException
	 */
	public static List<byte[]> readRecords(String filePath, Tuple tuple) throws IOException {
		List<byte[]> list = new ArrayList<>();
		try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
			stream.filter(s -> s.startsWith("[") && s.endsWith("]")).forEach(s -> {
				byte[] buffer = new byte[tuple.getLength()];
				s = s.substring(s.indexOf("[") + 1, s.indexOf("]"));
				String[] content = s.split(",");
				if (tuple.getAttributeNum() == content.length) {
					int offset = 0;
					int count = 0;
					for (TupleAttribute tupleAttribute : tuple.getTupleAttributes()) {
						String attrType = tupleAttribute.getType();
						int attrLen = tupleAttribute.getLength();
						if (attrType.equals("Integer") && attrLen == 4) {
							// if it is an integer
							ByteUtils.intToByteArray(Integer.parseInt(content[count].trim()), buffer, offset);
						} else if (attrType.equals("String")) {
							// if it is a string
							ByteUtils.stringToByteArray(content[count], buffer, offset, offset + attrLen);
						}
						count++;
						offset += attrLen;
					}
					list.add(buffer);
				}
			});
		}
		return list;
	}

	/**
	 * Prints the storage metadata info to the result file.
	 * 
	 * @param filePath
	 * @param lhFile
	 * @throws IOException
	 */
	public static void printStorageInfoToFile(String filePath, LinearHashFile lhFile) throws IOException {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(filePath))) {
			bw.write("Lienar hash storage.");
			bw.newLine();
			int hash = 0, pageId;
			for (ChainMetaData cmd : lhFile.getChainMetaDatas()) {
				pageId = cmd.getRootPageId();
				bw.write("Chain Hash: " + hash + " Chain Length:" + cmd.getLength());
				bw.newLine();
				bw.newLine();
				ChainIterator chainItr = new ChainIterator(pageId);
				while (chainItr.hasNext()) {
					byte[] record = chainItr.next();
					int key = ByteUtils.byteArrayToInt(record, 0);
					bw.write("Key:" + key);
					bw.newLine();
				}
				bw.newLine();
				hash++;
			}
		}
	}

	/**
	 * Unmarshalls object.
	 * 
	 * @param xmlFile
	 * @param valueType
	 * @return
	 * @throws JAXBException
	 */
	@SuppressWarnings("unchecked")
	public static <T> T unmarshallObject(String xmlFile, Class<T> valueType) throws JAXBException {
		File file = new File(xmlFile);
		JAXBContext jaxbContext = JAXBContext.newInstance(valueType);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		T t = (T) jaxbUnmarshaller.unmarshal(file);
		return t;
	}

	/**
	 * Marshalls object.
	 * 
	 * @param xmlFile
	 * @param obj
	 * @throws JAXBException
	 */
	public static <T> void marshallObject(String xmlFile, T obj) throws JAXBException {
		File file = new File(xmlFile);
		JAXBContext jaxbContext = JAXBContext.newInstance(obj.getClass());
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		jaxbMarshaller.marshal(obj, file);
	}

}
