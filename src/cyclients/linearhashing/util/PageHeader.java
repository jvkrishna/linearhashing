package cyclients.linearhashing.util;

import java.util.Arrays;

/**
 * Class for storing page header.F
 * 
 * @author Vamsi Krishna J
 *
 */
public class PageHeader {
	public static final int DEFAULT_PAGE_HEADER_SIZE = 32;
	private byte[] header;

	public enum HEADER_ATTR {
		HEADER_SIZE(0), CURRENT_PAGE(4), NEXT_PAGE(8), PAGE_TYPE(12), TUPLE_LENGTH(16), NUM_TUPLES(20), PAGE_SIZE(
				24), PAGE_ID(28);
		private int offset;

		private HEADER_ATTR(int offset) {
			this.offset = offset;
		}

		public int getOffset() {
			return offset;
		}
	}

	public PageHeader() {
		this(new byte[DEFAULT_PAGE_HEADER_SIZE]);
	}

	public PageHeader(byte[] header) {
		if (header.length > DEFAULT_PAGE_HEADER_SIZE) {
			header = Arrays.copyOf(header, DEFAULT_PAGE_HEADER_SIZE);
		}
		this.header = header;
	}

	public void setHeaderValue(HEADER_ATTR headerAttr, int value) {
		ByteUtils.intToByteArray(value, this.header, headerAttr.getOffset());
	}

	public int getHeaderValue(HEADER_ATTR headerAttr) {
		return ByteUtils.byteArrayToInt(this.header, headerAttr.getOffset());
	}

	public byte[] getHeader() {
		return this.header;
	}

}
