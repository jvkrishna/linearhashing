package cyclients.linearhashing.util;

import storagemanager.StorageDirectoryDocument;
import storagemanager.StorageManagerClient;
import storagemanager.StorageUtils;

/**
 * Singleton class to retrieve the storage clients.
 * 
 * @author Vamsi Krishna J
 *
 */
public class ApplicationConfig {

	private static ApplicationConfig instance = null;
	private StorageUtils storageUtils = null;
	private StorageManagerClient storageManagerClient;
	private StorageDirectoryDocument xmlParser;
	private boolean initialized = false;

	public static ApplicationConfig getInstance() {
		if (instance == null) {
			synchronized (ApplicationConfig.class) {
				if (instance == null)
					instance = new ApplicationConfig();
			}
		}
		return instance;
	}

	private ApplicationConfig() {
	};

	public void initialize(StorageUtils storageUtils) {
		if (!initialized) {
			this.storageUtils = storageUtils;
			this.storageManagerClient = storageUtils.getXmlClient();
			this.xmlParser = storageUtils.getXmlParser();
			initialized = true;
		}
	}

	public StorageUtils getStorageUtils() {
		return storageUtils;
	}

	public StorageManagerClient getStorageManagerClient() {
		return storageManagerClient;
	}

	public StorageDirectoryDocument getXmlParser() {
		return xmlParser;
	}

}
