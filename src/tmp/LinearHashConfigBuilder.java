package tmp;

import javax.xml.xpath.XPathExpressionException;

public class LinearHashConfigBuilder extends ConfigParser {

	public LinearHashConfigBuilder(String configFile) {
		super(configFile);
	}

	// TODO -- Update XPaths

	public int getRootPageId() {
		return Integer.parseInt(evaluteXPath("//BTreeConfig/RootPageId/text()"));
	}

	public int getSequencePageId() {
		return Integer.parseInt(evaluteXPath("//BTreeConfig/SequencePageId/text()"));
	}

	public int getPageSizeUsed() {
		return Integer.parseInt(evaluteXPath("//BTreeConfig/PageSizeUsed/text()"));
	}

	public String getLinearHashFileName() {
		return evaluteXPath("//BTreeConfig/LinearHashName/text()");
	}

	public int getMedianValue() {
		return Integer.parseInt(evaluteXPath("//BTreeConfig/Median/text()"));
	}

	public int getSpValue() {
		return Integer.parseInt(evaluteXPath("//BTreeConfig/SP/text()"));
	}

	public double getLowerBound() {
		return Double.parseDouble(evaluteXPath("//BTreeConfig/LowerBound/text()"));
	}

	public double getUpperBound() {
		return Double.parseDouble(evaluteXPath("//BTreeConfig/UpperBound/text()"));
	}
	
	public boolean updateRootPageId(int value) {
		return updateNode("//BTreeConfig/RootPageId/text()", value);
	}
	
	public boolean updateSequencePageId(int value) {
		return updateNode("//BTreeConfig/SequencePageId/text()", value);
	}
	
	public boolean updatePageSizeUsed(int value) {
		return updateNode("//BTreeConfig/PageSizeUsed/text()", value);
	}

	private String evaluteXPath(String expression) {
		try {
			return evalute(expression);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static void main(String[] args) {
		String path = "C:\\Users\\krishnaj\\Krishna\\coursework\\coms661\\linearhashing\\CyDIW_Root_bin_(2018-01-18)\\cyutils\\btree\\workspace\\MyBTreeConfigBulk256.xml";
		LinearHashConfigBuilder lhc = new LinearHashConfigBuilder(path);
		System.out.println(lhc.updatePageSizeUsed(280));
	}

}
