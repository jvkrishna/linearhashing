package tmp;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class ConfigParser {

	private Document doc;
	private XPath xpath;
	private String filePath;

	public ConfigParser(String str) {
		try {
			this.filePath = str;
			DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
			domFactory.setNamespaceAware(true);
			DocumentBuilder builder = domFactory.newDocumentBuilder();
			doc = builder.parse(str);

			XPathFactory factory = XPathFactory.newInstance();
			xpath = factory.newXPath();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected String evalute(String expression) throws XPathExpressionException {
		XPathExpression expr = xpath.compile(expression);
		return (String) expr.evaluate(doc, XPathConstants.STRING);
	}

	protected <T> boolean updateNode(String expression, T value) {
		try {
			XPathExpression expr = xpath.compile(expression);
			Node node = (Node) expr.evaluate(doc, XPathConstants.NODE);
			node.setTextContent(String.valueOf(value));
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result = new StreamResult(new File(this.filePath));
			transformer.transform(source, result);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
